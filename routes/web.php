<?php 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\SendEmail;




Route::get('/', function () {
    return view('welcome');
});

Route::get('/send/message','SmsController@sendMessage');

Route::get('/email',function () {

    Mail::to('sh.mamajon@gmail.com')->send(new SendEmail());
    return new SendEmail();
});

