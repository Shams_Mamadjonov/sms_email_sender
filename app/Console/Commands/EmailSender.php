<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EmailSender extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EmailSender';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending email to customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route()->named('/email')) {
        }

        return $next($request);
    }
}
