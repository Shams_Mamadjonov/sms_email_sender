<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Nexmo\Laravel\Facade\Nexmo;


class SmsController extends Controller
{
    public function sendMessage() 
    {
        Nexmo::message()->send([
        'from' => 'Shop',
        'to' => '+992550000736',
        'text' => 'Book, 58 Somoni'
        ]);

        echo "Message sent successfully";
    }
}
